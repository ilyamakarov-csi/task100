package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class HomePage {
    private WebDriver driver;
    private final String signOutLink =  ".sign-out-span>a";

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public LoginPage signOut() {
        driver.findElement(By.cssSelector(signOutLink)).click();
        return new LoginPage(driver);
    }

    public boolean isSignOutButtonVisible() {
        return driver.findElement(By.cssSelector(signOutLink)).isDisplayed();
    }
}
