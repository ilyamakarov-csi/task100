package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import ru.yandex.qatools.allure.annotations.Title;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
@Title("Login page test")
public class TestLoginPage {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Title("This is login test Title")
    @Description("This is login test Description")
    @TestCaseId("LoginID")
    @Test
    public void loginTest() {
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.login("IlyaMakarov", "Password1");
        Assert.assertTrue(homePage.isSignOutButtonVisible(), "Sign out button is not displayed!");
    }
}
