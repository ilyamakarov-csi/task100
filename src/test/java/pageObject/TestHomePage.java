package pageObject;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import ru.yandex.qatools.allure.annotations.Title;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
@Title("Home page test")
@Listeners({ ListenerClass.class })
public class TestHomePage {

    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Title("This is home test Title")
    @Description("This is home test Description")

    @TestCaseId("LogoutID")
    @Test
    public void logoutTest(ITestContext testContext) {
        testContext.setAttribute("WebDriver", this.driver);
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.login("IlyaMakarov", "Password1");
        loginPage = homePage.signOut();
        // should be true
        //Assert.assertTrue(loginPage.isSubmitButtonDisplayed(), "Submit button is not displayed");
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();

        Assert.fail("FAILED. Noooooooooooooo!" + cap.getBrowserName() + " " + cap.getVersion());
    }
}
