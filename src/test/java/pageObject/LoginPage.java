package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class LoginPage {

    private WebDriver driver;
    private final String address = "https://192.168.100.26/";
    private final String userNameInputID = "Username";
    private final String passwordInputID = "Password";
    private final String submitButtonID = "SubmitButton";

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        driver.get(address);
    }

    public HomePage login(String login, String password) {
        driver.findElement(By.id(userNameInputID)).sendKeys(login);
        driver.findElement(By.id(passwordInputID)).sendKeys(password);
        driver.findElement(By.id(submitButtonID)).click();

        return new HomePage(driver);
    }

    public boolean isSubmitButtonDisplayed() {
        return driver.findElement(By.id(submitButtonID)).isDisplayed();
    }
}
